/*
Une actualité est composée de :
- Un identifiant : id
- Un titre : title
- Une description : description
- Le nom du lieu : location
- Une date de début : dateStart
- Une date de fin : dateEnd
- Un tarif : price

destructuring : props
décomposition / assignation par décomposition
déstructurer
function Article(props) {
  const { title, description, location, dateStart, dateEnd, price } = props;
}
*/
function Article({ title, description, location, dateStart, dateEnd, price }) {
  return (
    <section>
      <h1>{title}</h1>
      <p>{description}</p>
      <p>{location}</p>
      <p>{dateStart}</p>
      <p>{dateEnd}</p>
      <p>{price}</p>
    </section>
  );
}

export default Article;
