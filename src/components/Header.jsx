function Header() {
  const navigation = ["Accueil", "Agenda", "Nous contacter"];
  // JSX - titre, nav : accueil, agenda, nous contacter
  return (
    <>
      <h1>Toulews</h1> {/* Toulouse News */}
      <nav>
        <ul>
          {/* key */}
          {navigation.map((eachItem, index) => {
            return <li key={index}>{eachItem}</li>;
          })}
        </ul>
      </nav>
    </>
  );
}

export default Header;
