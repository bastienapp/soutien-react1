/*

L'agenda affichera la liste des actualités (basée sur un tableau d'actualités "en dur").
*/
import Article from "./Article";

function Agenda() {
  const news = [
    {
      id: 12,
      title: "Musée du tacos",
      description: "Musée et dégustation",
      location: "Ramonville",
      dateStart: "2024-03-22",
      dateEnd: "2024-06-02",
      price: "12,50 €",
    },
    {
      id: 9,
      title: "Concert Red Hot Chili Peppers",
      description: "Venez vous amuser avec votre groupe préféré gratuitement",
      location: "Montpellier - RockStore",
      dateStart: "2024-01-30",
      dateEnd: "2024-01-30",
      price: "0 €",
    },
    {
      id: 77,
      title: "Salon du jeu",
      description: "Tournoi de jeux de société",
      location: "Toulouse - Chepa",
      dateStart: "30/01/24",
      dateEnd: "30/01/24",
      price: "5 €",
    },
    {
      id: 17,
      title: "PAGS",
      description: "Pau game show",
      location: "Pau",
      dateStart: "2024-03-13",
      dateEnd: "2024-03-15",
      price: "10 €",
    },
  ];
  const fruitList = ["Ananas", "Pomme", "Kiwi"]
  const [fruit1, fruit2, fruit3] = fruitList;

  console.log(fruitList); // Array(3) [ "Ananas", "Pomme", "Kiwi" ]
  console.log(...fruitList); // Ananas Pomme Kiwi

  return (
    <>
      {news.map((article) => {
        return (
          <Article
            key={article.id}
            {...article}
          />
        );
      })}
    </>
  );
}

export default Agenda;
